package transaction.script.constants;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.platform.utils.PostgresDBUtil;

public class AppConstants {
	private static final Logger LOGGER = LoggerFactory.getLogger(PostgresDBUtil.class);
	static Properties properties;
	static {
		properties = new Properties();
		try {
			InputStream inputStream = AppConstants.class.getClassLoader().getResourceAsStream("platform.properties");
			properties.load(inputStream);
		} catch (IOException e) {
			e.printStackTrace();
			LOGGER.error("<< Error loading properties file:");

		}
	}

	public static final String POSTGRES_IP = properties.getProperty("postgres_host");
	public static final String TRANSACTION = "TRANSACTION";
	//Dynamo Constants
	public static final String DYNAMO_ENVIRONMENT = properties.getProperty("dynamo_environment");
	public static final String DYNAMO_FROM = properties.getProperty("dynamo_from");
	public static final String DYNAMO_HOST = properties.getProperty("dynamo_host");
	public static final String AWS_ACCESSKEY = properties.getProperty("aws_accessKey");
	public static final String AWS_SECRETKEY = properties.getProperty("aws_secretKey");
	//Table Names for postgres
	public static final String TRADER_TABLE_NAME = "kal_traderapp_transaction";
	public static final String CA_TABLE_NAME = "kal_ca_transaction";
	public static final String FARMER_TABLE_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_TABLE_NAME = "kal_ai_transaction";
	public static final String SOCIETY_TABLE_NAME = "kal_cos_transaction";
	public static final String TRANSPORTER_TABLE_NAME = "kal_transporterapp_transaction";
	public static final String WAREHOUSER_TABLE_NAME = "kal_wh_transaction";
	//Collection Names for mongo
	public static final String TRADER_COLLECTION_NAME = "kal_traderapp_transaction";
	public static final String CA_COLLECTION_NAME = "kal_ca_transaction";
	public static final String FARMER_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String INPUTS_COLLECTION_NAME = "kal_fertilizerapp_transaction";
	public static final String SOCIETY_COLLECTION_NAME = "kal_farmerapp_transaction";
	public static final String TRANSPORTER_COLLECTION_NAME = "kal_transporter_transaction";
	public static final String WAREHOUSER_COLLECTION_NAME = "kal_warehouseapp_transaction";
	//Biz Types
	public static final String TRADER = "traderapp";
	public static final String CA = "caapp";
	public static final String FARMER = "farmerapp";
	public static final String INPUTS = "aiapp";
	public static final String SOCIETY = "cosapp";
	public static final String TRANSPORTER = "transporterapp";
	public static final String WAREHOUSE = "warehouse";
	public static final String MONGO_HOST_ONE = properties.getProperty("mongo_host");
	public static final int MONGO_HOST_PORT = Integer.parseInt(properties.getProperty("mongo_port"));
	public static final String MONGO_HOST_TWO = properties.getProperty("mongo_host_two");
	public static final String MONGO_DATABASE = properties.getProperty("common_mongo_db_name");
	public static final String MONGO_USER = properties.getProperty("mongo_user");
	public static final String MONGO_PASSWORD = properties.getProperty("mongo_password");
	//Data Correction Constants
	public static final String DUPLICATE = "DUPLICATE";//Production status
	public static final String ACTIVE = "ACTIVE";//Production status
	public static final String DUPLICATE_ALL = "DUPLICATE_ALL";//Local mongo status
	public static final String DUPLICATE_DATA = "DUPLICATE_DATA";//Local mongo status
	public static final String DUPLICATE_MOBILE = "DUPLICATE_MOBILE";//Local mongo status
	public static final String IN_QUEUE = "IN_QUEUE";//Local mongo && production status
	public static final String FAIL_DUP_MOBILE = "FAIL_DUP_MOBILE";
	public static final String FAIL_EXCEPTION = "FAIL_EXCEPTION";
	public static final String DATA_SHEET = "DATA";
	//BusinessType Constants
	public static final String BUSINESS_TYPE_NAME_AGRITRADER = "AgriTrader";
	public static final String BUSINESS_TYPE_NAME_AGRIBROKER = "Broker";
	public static final String BUSINESS_TYPE_NAME_FARMER = "Farmer";
	public static final String BUSINESS_TYPE_NAME_TRANSPORTER = "Transporter";
	public static final String BUSINESS_TYPE_NAME_AGRIINPUT = "AgriInputs";
	public static final String BUSINESS_TYPE_NAME_AGRISOCIETY = "AgriCoop";
	public static final String BUSINESS_TYPE_NAME_WAREHOUSE = "Warehouser";
	public static final String BUSINESS_TYPE_ID_AGRITRADER = "BT000000";
	public static final String BUSINESS_TYPE_ID_AGRIBROKER = "BT000001";
	public static final String BUSINESS_TYPE_ID_FARMER = "BT000002";
	public static final String BUSINESS_TYPE_ID_TRANSPORTER = "BT000003";
	public static final String BUSINESS_TYPE_ID_AGRIINPUT = "BT000008";
	public static final String BUSINESS_TYPE_ID_AGRISOCIETY = "BT000010";
	public static final String BUSINESS_TYPE_ID_WAREHOUSE = "BT000006";
	//Validation constants
	public static final String INVALID = "INVALID";
	public static final String INVALID_NAME = "Name is invalid.";
	public static final String INVALID_BIZ_NAME = "Biz name is invalid.";
	public static final String INVALID_MOBILE = "Name is invalid.";
	public static final String INVALID_BIZ_TYPE = "Name is invalid.";
	public static final String INVALID_LOCATION = "Name is invalid.";
	public static final Map<String, String> MOBILE_TELECOM_CODES_MAP;
	static {
		MOBILE_TELECOM_CODES_MAP = new HashMap<String, String>();
		MOBILE_TELECOM_CODES_MAP.put("+254", "9");
		MOBILE_TELECOM_CODES_MAP.put("+237", "9");
		MOBILE_TELECOM_CODES_MAP.put("+243", "9");
		MOBILE_TELECOM_CODES_MAP.put("+251", "9");
		MOBILE_TELECOM_CODES_MAP.put("+233", "9");
		MOBILE_TELECOM_CODES_MAP.put("+225", "9");
		MOBILE_TELECOM_CODES_MAP.put("+213", "9");
		MOBILE_TELECOM_CODES_MAP.put("+261", "9");
		MOBILE_TELECOM_CODES_MAP.put("+258", "9");
		MOBILE_TELECOM_CODES_MAP.put("+234", "10");
		MOBILE_TELECOM_CODES_MAP.put("+242", "9");
		MOBILE_TELECOM_CODES_MAP.put("+255", "9");
		MOBILE_TELECOM_CODES_MAP.put("+256", "9");
		MOBILE_TELECOM_CODES_MAP.put("+63", "10");
		MOBILE_TELECOM_CODES_MAP.put("+880", "10");
		MOBILE_TELECOM_CODES_MAP.put("+62", "9,10,11");
		MOBILE_TELECOM_CODES_MAP.put("+95", "8");
		MOBILE_TELECOM_CODES_MAP.put("+977", "10");
		MOBILE_TELECOM_CODES_MAP.put("+92", "10");
		MOBILE_TELECOM_CODES_MAP.put("+91", "10");
		MOBILE_TELECOM_CODES_MAP.put("+966", "9");
		MOBILE_TELECOM_CODES_MAP.put("+94", "7");
		MOBILE_TELECOM_CODES_MAP.put("+84", "10");
		MOBILE_TELECOM_CODES_MAP.put("+66", "9");
		MOBILE_TELECOM_CODES_MAP.put("+31", "9");
		MOBILE_TELECOM_CODES_MAP.put("+40", "9");
		MOBILE_TELECOM_CODES_MAP.put("+46", "9");
	}
	public static final Map<String, String> COUNTRY_CODES;
	static {
		COUNTRY_CODES = new HashMap<String, String>();
		COUNTRY_CODES.put("Kenya", "+254");
		COUNTRY_CODES.put("Cameroon", "+237");
		COUNTRY_CODES.put("DemRepCongo", "+243");
		COUNTRY_CODES.put("Ethiopia", "+251");
		COUNTRY_CODES.put("Ghana", "+233");
		COUNTRY_CODES.put("IvoryCoast", "+225");
		COUNTRY_CODES.put("Algeria", "+213");
		COUNTRY_CODES.put("Madagascar", "+261");
		COUNTRY_CODES.put("Mozambique", "+258");
		COUNTRY_CODES.put("Nigeria", "+234");
		COUNTRY_CODES.put("RepCongo", "+242");
		COUNTRY_CODES.put("Tanzania", "+255");
		COUNTRY_CODES.put("Uganda", "+256");
		COUNTRY_CODES.put("Philippines", "+63");
		COUNTRY_CODES.put("Bangladesh", "+880");
		COUNTRY_CODES.put("Indonesia", "+62");
		COUNTRY_CODES.put("Myanmar", "+95");
		COUNTRY_CODES.put("Nepal", "+977");
		COUNTRY_CODES.put("Pakistan", "+92");
		COUNTRY_CODES.put("India", "+91");
		COUNTRY_CODES.put("SaudiArabia", "+966");
		COUNTRY_CODES.put("SriLanka", "+94");
		COUNTRY_CODES.put("Vietnam", "+84");
		COUNTRY_CODES.put("Thailand", "+66");
		COUNTRY_CODES.put("Netherlands", "+31");
		COUNTRY_CODES.put("Romania", "+40");
		COUNTRY_CODES.put("Sweden", "+46");
	}
	public static final Map<String, String> COUNTRY_IDS;
	static {
		COUNTRY_IDS = new HashMap<String, String>();
		COUNTRY_CODES.put("Kenya", "101");
		COUNTRY_CODES.put("Cameroon", "107");
		COUNTRY_CODES.put("DemRepCongo", "112");
		COUNTRY_CODES.put("Ethiopia", "117");
		COUNTRY_CODES.put("Ghana", "120");
		COUNTRY_CODES.put("IvoryCoast", "123");
		COUNTRY_CODES.put("Algeria", "124");
		COUNTRY_CODES.put("Madagascar", "128");
		COUNTRY_CODES.put("Mozambique", "135");
		COUNTRY_CODES.put("Nigeria", "138");
		COUNTRY_CODES.put("RepCongo", "139");
		COUNTRY_CODES.put("Tanzania", "150");
		COUNTRY_CODES.put("Uganda", "153");
		COUNTRY_CODES.put("Philippines", "301");
		COUNTRY_CODES.put("Bangladesh", "303");
		COUNTRY_CODES.put("Indonesia", "314");
		COUNTRY_CODES.put("Myanmar", "329");
		COUNTRY_CODES.put("Nepal", "330");
		COUNTRY_CODES.put("Pakistan", "333");
		COUNTRY_CODES.put("India", "335");
		COUNTRY_CODES.put("SaudiArabia", "336");
		COUNTRY_CODES.put("SriLanka", "340");
		COUNTRY_CODES.put("Vietnam", "349");
		COUNTRY_CODES.put("Thailand", "377");
		COUNTRY_CODES.put("Netherlands", "436");
		COUNTRY_CODES.put("Romania", "440");
		COUNTRY_CODES.put("Sweden", "448");
	}
}