package transaction.script.excelSheet.validators;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import transaction.script.constants.AppConstants;
import transaction.script.platform.utils.MongoDBUtil;

import com.google.gson.Gson;
import com.mongodb.DB;
import com.mongodb.DBCollection;

public class ExcelSheetsValidator {
	public static final Logger LOGGER = LoggerFactory.getLogger(ExcelSheetsValidator.class);
	static DB db = null;
	static DBCollection dumpingCollection = null;
	static DBCollection masterCollection = null;
	static Gson gson = null;
	static int numberOfUsersEligibleForDeletion = 0;
	static String countryName, businessTypeName;
	static {
		try {
			db = MongoDBUtil.getConnection();
			dumpingCollection = db.getCollection("EligibleSept28test");
			masterCollection = db.getCollection("abhimastertest");
			gson = new Gson();
		} catch (Exception e) {
			LOGGER.error("Unable to initialise scripts");
			e.printStackTrace();
			System.exit(1);
		}
	}

	public static void main(String[] args1) {
		String args[] = new String[2];
		args[0] = "as";
		args[1] = "/home/abhi/Desktop/Vietnam_AgriTrader_904.xlsx";
		validate(args);
	}

	public static void validate(String... args) {
		String sheetNameWithLocation = args[1];
		Workbook workbook = null;
		try (FileInputStream inputStream = new FileInputStream(sheetNameWithLocation);) {
			workbook = new XSSFWorkbook(inputStream);
			createSheets(workbook);
			/*@SuppressWarnings("unchecked")
			List<String> mobileNumbersList = masterCollection.distinct("mobileNum");
			Set<String> processedMobileNumbers = new HashSet<String>(mobileNumbersList);*/
			Sheet dataSheet = workbook.getSheet(AppConstants.DATA_SHEET);
			if (dataSheet != null) {
				countryName = dataSheet.getSheetName().split("_")[0];
				businessTypeName = dataSheet.getSheetName().split("_")[1];
				for (int i = 1; i <= dataSheet.getLastRowNum(); i++) {
					System.out.println(i + " " + readData(dataSheet, i).get(1));
					if (i == 2)
						break;
				}
			} else {
				LOGGER.error("DATA sheet is not present. Program exiting....");
			}
		} catch (FileNotFoundException e) {
			LOGGER.error("Unable to locate file");
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.error("Unable to load or write to file.");
			e.printStackTrace();
		} finally {
			if (workbook != null) {
				try (FileOutputStream outputStream = new FileOutputStream(sheetNameWithLocation);) {
					workbook.write(outputStream);
				} catch (FileNotFoundException e) {
					LOGGER.error("Unable to locate file");
					e.printStackTrace();
				} catch (IOException e) {
					LOGGER.error("Unable to load or write to file.");
					e.printStackTrace();
				}
			}
		}
	}

	private static HashMap<String, Sheet> createSheets(Workbook workbook) {
		HashMap<String, Sheet> sheetsMap = new HashMap<String, Sheet>();
		if (workbook.getSheet(AppConstants.DUPLICATE_ALL) == null)
			sheetsMap.put(AppConstants.DUPLICATE_ALL, workbook.createSheet(AppConstants.DUPLICATE_ALL));
		if (workbook.getSheet(AppConstants.DUPLICATE_MOBILE) == null)
			sheetsMap.put(AppConstants.DUPLICATE_MOBILE, workbook.createSheet(AppConstants.DUPLICATE_MOBILE));
		if (workbook.getSheet(AppConstants.DUPLICATE_DATA) == null)
			sheetsMap.put(AppConstants.DUPLICATE_DATA, workbook.createSheet(AppConstants.DUPLICATE_DATA));
		if (workbook.getSheet(AppConstants.IN_QUEUE) == null)
			sheetsMap.put(AppConstants.IN_QUEUE, workbook.createSheet(AppConstants.IN_QUEUE));
		return sheetsMap;
	}

	private static ArrayList<String> readData(Sheet dataSheet, int rowNumber) {
		String errorMessage = "";
		ArrayList<String> data = new ArrayList<String>();
		Row row = dataSheet.getRow(rowNumber);
		//Name
		if (row.getCell(0) != null) {
			if (!row.getCell(0).getStringCellValue().trim().equals(""))
				data.add(row.getCell(0).getStringCellValue().trim());
			else {
				data.add(AppConstants.INVALID);
				errorMessage = errorMessage.concat(AppConstants.INVALID_NAME);
			}
		} else {
			data.add(AppConstants.INVALID);
			errorMessage = errorMessage.concat(AppConstants.INVALID_NAME);
		}
		//BizName
		if (row.getCell(1) != null) {
			if (!row.getCell(1).getStringCellValue().trim().equals(""))
				data.add(row.getCell(1).getStringCellValue().trim());
			else {
				data.add(AppConstants.INVALID);
				errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_NAME);
			}
		} else {
			data.add(AppConstants.INVALID);
			errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_NAME);
		}
		//Mobile with telecome code
		if (row.getCell(2) != null) {
			if (!row.getCell(2).getStringCellValue().trim().equals("")) {
				//Check validity of number--------------
				data.add(row.getCell(2).getStringCellValue().trim());
			} else {
				//Generate dummy mobile number---------------------
				data.add(AppConstants.INVALID);
				errorMessage = errorMessage.concat(AppConstants.INVALID_MOBILE);
			}
		} else {
			//Generate dummy mobile number---------------------
			data.add(AppConstants.INVALID);
			errorMessage = errorMessage.concat(AppConstants.INVALID_MOBILE);
		}
		//BizTypeName
		if (row.getCell(3) != null) {
			String bizTypeId = getBizTypeId(row.getCell(3).getStringCellValue().trim());
			if (!bizTypeId.equals(AppConstants.INVALID))
				data.add(bizTypeId);
			else {
				data.add(AppConstants.INVALID);
				errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_TYPE);
			}
		} else {
			data.add(AppConstants.INVALID);
			errorMessage = errorMessage.concat(AppConstants.INVALID_BIZ_TYPE);
		}
		//Location
		if (row.getCell(4) != null) {
			data.add(row.getCell(4).getStringCellValue());
		} else {
			data.add(AppConstants.INVALID);
		}
		//Products
		if (row.getCell(5) != null)
			data.add(row.getCell(5).getStringCellValue());
		else
			data.add("");
		//Email
		if (row.getCell(6) != null)
			data.add(row.getCell(6).getStringCellValue());
		else
			data.add("");
		//Website
		if (row.getCell(7) != null)
			data.add(row.getCell(7).getStringCellValue());
		else
			data.add("");
		//Address
		if (row.getCell(8) != null)
			data.add(row.getCell(8).getStringCellValue());
		else
			data.add("");
		//Other contact numbers
		if (row.getCell(9) != null)
			data.add(row.getCell(9).getStringCellValue());
		else
			data.add("");
		return data;
	}

	private static String getBizTypeId(String bizTypeName) {
		bizTypeName = bizTypeName.toLowerCase();
		switch (bizTypeName) {
		case AppConstants.BUSINESS_TYPE_NAME_AGRITRADER:
			return AppConstants.BUSINESS_TYPE_ID_AGRITRADER;
		case AppConstants.BUSINESS_TYPE_NAME_AGRIBROKER:
			return AppConstants.BUSINESS_TYPE_ID_AGRIBROKER;
		case AppConstants.BUSINESS_TYPE_NAME_FARMER:
			return AppConstants.BUSINESS_TYPE_ID_FARMER;
		case AppConstants.BUSINESS_TYPE_NAME_TRANSPORTER:
			return AppConstants.BUSINESS_TYPE_ID_TRANSPORTER;
		case AppConstants.BUSINESS_TYPE_NAME_AGRIINPUT:
			return AppConstants.BUSINESS_TYPE_ID_AGRIINPUT;
		case AppConstants.BUSINESS_TYPE_NAME_AGRISOCIETY:
			return AppConstants.BUSINESS_TYPE_ID_AGRISOCIETY;
		case AppConstants.BUSINESS_TYPE_NAME_WAREHOUSE:
			return AppConstants.BUSINESS_TYPE_ID_WAREHOUSE;
		default:
			return AppConstants.INVALID;
		}
	}

	private static String getDummyMobileNumber(String countryName) {
		return null;
	}
}