package transaction.script.models;

public class LocaleNameTo {
	private int languageId;
	private String languageName;
	private String displayName;
	
	/**
	 * @return the languageName
	 */
	public String getLanguageName() {
		return languageName;
	}
	/**
	 * @param languageName the languageName to set
	 */
	public void setLanguageName(String languageName) {
		this.languageName = languageName;
	}
	/**
	 * @return the languageId
	 */
	public int getLanguageId() {
		return languageId;
	}
	/**
	 * @param languageId the languageId to set
	 */
	public void setLanguageId(int languageId) {
		this.languageId = languageId;
	}
	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}
	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj instanceof LocaleNameTo ){
			return ((LocaleNameTo) obj).languageName.equals(languageName) && ((LocaleNameTo) obj).languageId == languageId;
		}else{
			return false;
		}
	}
}
