package transaction.script.models;

public class UserBusinessDetails {
	private String businessName;
	private String businessTypeId;
	private PlaceTo locationTo;
	private String businessKey;

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getBusinessTypeId() {
		return businessTypeId;
	}

	public void setBusinessTypeId(String businessTypeId) {
		this.businessTypeId = businessTypeId;
	}

	public PlaceTo getLocationTo() {
		return locationTo;
	}

	public void setLocationTo(PlaceTo locationTo) {
		this.locationTo = locationTo;
	}

	public String getBusinessKey() {
		return businessKey;
	}

	public void setBusinessKey(String businessKey) {
		this.businessKey = businessKey;
	}
}