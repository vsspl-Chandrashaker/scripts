/*
 * Copyright (c) 2014 Vasudhaika Software Solutions Pvt Ltd.
 * All rights reserved.
 *
 * This code is the confidential and proprietary information of   
 * Vasudhaika Software Solutions Pvt Ltd. You shall not disclose
 * such Confidential Information and shall use it only in accordance
 * with the terms of the license agreement you entered into with 
 * Vasudhaika Software Solutions Pvt Ltd.
 */
package transaction.script.platform.utils;

import java.sql.Connection;
import java.sql.DriverManager;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

import transaction.script.constants.PlatformConstants;

public class HsqlDBUtil {
	private static final org.slf4j.Logger LOGGER = org.slf4j.LoggerFactory.getLogger(HsqlDBUtil.class);
	public static Connection dbConnection = null;
	private static Client client = ClientBuilder.newClient();

	public static Client client() {
		return client;
	}

	static {
		try {
			getConnection();
		} catch (Exception e) {

			LOGGER.error("Unable to connect to in memory database", e);
			e.printStackTrace();
		}
	}

	private HsqlDBUtil() {
	}

	private static void getConnection() {
		try {
			Class.forName(PlatformConstants.INMEMORY_COMMONDB_DRIVER);
			dbConnection = DriverManager.getConnection(PlatformConstants.INMEMORY_COMMONDB_URL, PlatformConstants.INMEMORY_COMMONDB_USER, PlatformConstants.INMEMORY_COMMONDB_PASSWORD);
		} catch (Exception e) {
			LOGGER.error("Error in the DataBase Connection", e);
			e.printStackTrace();
		}
	}

	public static String cloudFrontCommonDataImageUrl(String productImageUrl) {
		productImageUrl = productImageUrl.replace(PlatformConstants.COMMONDATA_CLOUDFRONT_URL, PlatformConstants.COMMONDATA_S3_URL);
		return productImageUrl.split(",")[0];
	}
}
